package rythmmixpackage.rythmmix;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;


public class SplashScreenActivity extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splashscreen);
        Globals.context = getApplicationContext();

        Handler handler1 = new Handler();
        Handler handler2 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override

            public void run() {
                final String PREFS_NAME = "MyPrefsFile";

                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

                if (settings.getBoolean("my_first_time", true)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreenActivity.this);
                    alertDialog.setTitle("Primeira vez");
                    alertDialog.setMessage("É a primeira vez que esta a correr a aplicação, deseja correr o tutorial?");
                    alertDialog.setIcon(R.drawable.appicon1);
                    alertDialog.setPositiveButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intPrincipal = new Intent(Globals.context, MenuPrincipalActivity.class);
                            startActivity(intPrincipal);

                            finish();
                            dialog.cancel();

                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intTutorial = new Intent(Globals.context, TutorialSegurarActivity.class);
                            startActivity(intTutorial);
                            finish();

                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();


                    settings.edit().putBoolean("my_first_time", false).commit();
                } else {
                    Intent intMenuPrincipal2 = new Intent(Globals.context, MenuPrincipalActivity.class);
                    startActivity(intMenuPrincipal2);
                    finish();
                }
            }
        }, 5000);


    }
}