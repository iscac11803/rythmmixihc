package rythmmixpackage.rythmmix;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;


public class TutorialMaracasActivity extends Activity implements SensorEventListener {
    private final float NOISE = (float) 2.0;
    private float mLastX;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private MediaPlayer mpmaracas, aplause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tutorial_maracas);
        Globals.context = getApplicationContext();
        Globals.aplauso2 = false;
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mpmaracas = MediaPlayer.create(getApplicationContext(), R.raw.maracawav);

        Button btnAvancar = (Button) findViewById(R.id.btn_pandeireta_avan);
        Globals.avancar = false;

        btnAvancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intIrParaTutorialBongo = new Intent(Globals.context, TutorialBongoActivity.class);
                startActivity(intIrParaTutorialBongo);
                finish();
            }
        });

        if (!Globals.avancar) {
            btnAvancar.setTextColor(Color.parseColor("#FF656565"));
            btnAvancar.setClickable(false);
        }
        final AlertDialog.Builder alertadd = new AlertDialog.Builder(
                TutorialMaracasActivity.this);
        LayoutInflater factory = LayoutInflater.from(TutorialMaracasActivity.this);
        final View view = factory.inflate(R.layout.imagemdialogmaraca, null);
        alertadd.setView(view);
        alertadd.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {
                dlg.cancel();


            }
        });
        alertadd.show();

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
// can be safely ignored for this demo
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Button btnAvancar = (Button) findViewById(R.id.btn_pandeireta_avan);
        float x = event.values[0];

        if (!mInitialized) {
            mLastX = x;


            mInitialized = true;
        } else {
            float deltaX = Math.abs(mLastX - x);

            if (deltaX < NOISE) deltaX = (float) 0.0;

            mLastX = x;


            if (Globals.avancar) {
                if (!(Globals.aplauso2)) {
                    Toast.makeText(Globals.context, "Muito bem!",
                            Toast.LENGTH_SHORT).show();
                    Globals.aplauso2 = true;

                }
                btnAvancar.setClickable(true);
                btnAvancar.setTextColor(Color.parseColor("#FF030303"));
            }
            if (deltaX > 6) {
                mpmaracas.start();
                Globals.avancar = true;
            }
               /* if(deltaX >40) {
                    mpmaracas2.start();
                    Globals.avancar=true;
                }*/


        }
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}

