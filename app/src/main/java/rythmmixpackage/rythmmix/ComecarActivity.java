package rythmmixpackage.rythmmix;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class ComecarActivity extends Activity implements SensorEventListener {
    private final float NOISE = (float) 2.0;
    private float mLastX, mLastY;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private MediaPlayer mpmaracas, mpbombo, pandeireta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_comecar);
        RelativeLayout ecratocar = (RelativeLayout) findViewById(R.id.viewprincipal);
        Globals.context = getApplicationContext();
        Globals.aplauso = false;
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mpmaracas = MediaPlayer.create(Globals.context, R.raw.maracawav);
        //pandeireta = MediaPlayer.create(Globals.context, R.raw.toque_simples_pandeireta);
        mpbombo = MediaPlayer.create(Globals.context, R.raw.bombo);
        ImageView ivmaraca = (ImageView) findViewById(R.id.idcruzmaracas);
        ImageView ivpand = (ImageView) findViewById(R.id.idcruzpandeireta);
        ImageView ivbombo = (ImageView) findViewById(R.id.idcruzbombo);
        ImageView ivcoiso = (ImageView) findViewById(R.id.idcoiso);
        Globals.avancar = false;
        if (Globals.bomboativado == false) {
            ivbombo.setVisibility(View.VISIBLE);
        } else {
            ivbombo.setVisibility(View.INVISIBLE);
        }
        if (Globals.pandeiretaativada == false) {
            ivpand.setVisibility(View.VISIBLE);
        } else {
            ivpand.setVisibility(View.INVISIBLE);
        }
        if (Globals.maracasativadas == false) {
            ivmaraca.setVisibility(View.VISIBLE);
        } else {
            ivmaraca.setVisibility(View.INVISIBLE);
        }
        ivcoiso.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (Globals.pandeiretaativada == true) {
                    MediaPlayer mp = MediaPlayer.create(Globals.context, R.raw.toque_simples_pandeireta);
                    mp.start();

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();

                    }


                });
                }
            }
        });


    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
// can be safely ignored for this demo
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float x = event.values[0];
        float y = event.values[1];
        if (!mInitialized) {
            mLastX = x;
            mLastY = y;


            mInitialized = true;
        } else {
            float deltaX = Math.abs(mLastX - x);
            float deltaY = Math.abs(mLastY - y);
            if (deltaX < NOISE) deltaX = (float) 0.0;
            if (deltaY < NOISE) deltaY = (float) 0.0;
            mLastX = x;
            mLastY = y;

            if (Globals.maracasativadas == true) {
                if (deltaX > 25 && deltaY < 25) {
                    mpmaracas.start();

                }
            /*if (deltaX > 40) {
                mpmaracas1.start();

            }*/
            }
            if (Globals.bomboativado == true) {
                if (deltaY > 15 && deltaX < 25) {
                    mpbombo.start();

                }
            }
        }

    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

}
