package rythmmixpackage.rythmmix;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;


public class EscolherInstrumentosActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_escolherinsctrumentos);

        Globals.context = getApplicationContext();
        final Switch swmaracas = (Switch) findViewById(R.id.idmaracas);
        final Switch swpand = (Switch) findViewById(R.id.idpandeireta);
        final Switch swbombo = (Switch) findViewById(R.id.idbombo);
        swmaracas.setChecked(Globals.maracasativadas);
        swpand.setChecked(Globals.pandeiretaativada);
        swbombo.setChecked(Globals.bomboativado);
        Button btnFeito = (Button) findViewById(R.id.btn_feito);
        btnFeito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swmaracas.isChecked()) {
                    Globals.maracasativadas = true;
                } else
                    Globals.maracasativadas = false;
                if (swpand.isChecked()) {
                    Globals.pandeiretaativada = true;
                } else
                    Globals.pandeiretaativada = false;
                if (swbombo.isChecked()) {
                    Globals.bomboativado = true;
                } else
                    Globals.bomboativado = false;

                Intent intTutorial = new Intent(Globals.context, MenuPrincipalActivity.class);
                startActivity(intTutorial);
            }
        });
    }


}
