package rythmmixpackage.rythmmix;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;


public class TutorialBongoActivity extends Activity implements SensorEventListener {
    private final float NOISE = (float) 2.0;
    private float mLastY;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private MediaPlayer bombo, aplause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tutorial_bongo);
        Globals.context = getApplicationContext();
        Globals.aplauso = false;
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        bombo = MediaPlayer.create(getApplicationContext(), R.raw.bombo);

        aplause = MediaPlayer.create(getApplicationContext(), R.raw.aplause);
        Button btnAvancar = (Button) findViewById(R.id.btn_pandeireta_avan);
        Globals.avancar = false;

        btnAvancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intIrParaMenuPrincipal = new Intent(Globals.context, MenuPrincipalActivity.class);
                startActivity(intIrParaMenuPrincipal);
                finish();
            }
        });

        if (!Globals.avancar) {
            btnAvancar.setTextColor(Color.parseColor("#FF656565"));
            btnAvancar.setClickable(false);
        }
        final AlertDialog.Builder alertadd = new AlertDialog.Builder(
                TutorialBongoActivity.this);
        LayoutInflater factory = LayoutInflater.from(TutorialBongoActivity.this);
        final View view = factory.inflate(R.layout.imagemdialogbongo, null);
        alertadd.setView(view);
        alertadd.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {
                dlg.cancel();


            }
        });
        alertadd.show();


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
// can be safely ignored for this demo
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Button btnAvancar = (Button) findViewById(R.id.btn_pandeireta_avan);
        float y = event.values[1];

        if (!mInitialized) {
            mLastY = y;


            mInitialized = true;
        } else {
            float deltaY = Math.abs(mLastY - y);

            if (deltaY < NOISE) deltaY = (float) 0.0;

            mLastY = y;


            if (Globals.avancar) {
                if (!(Globals.aplauso)) {
                    Toast.makeText(Globals.context, "Terminou o tutorial! Parabens!",
                            Toast.LENGTH_LONG).show();
                    MediaPlayer mp2 = MediaPlayer.create(Globals.context, R.raw.aplause);
                    mp2.start();
                    Globals.aplauso = true;

                }
                btnAvancar.setClickable(true);
                btnAvancar.setTextColor(Color.parseColor("#FF030303"));
            }
            if (deltaY > 6) {
                bombo.start();

                Globals.avancar = true;
            }


        }
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}

